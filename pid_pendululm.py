"""
This file may not be shared/redistributed without permission. Please read copyright notice in the git repo. If this file contains other copyright notices disregard this text.
"""
import os
import numpy as np
from irlc import savepdf
from irlc.ex04.pid import PID
from irlc import Agent
import time
from fuzzy_project.Fuzzy_PID import Fuzzy_PID

class FuzzyPIDPendulumAgent(Agent):
    def __init__(self, env, v_target = 1):
        # Define fuzzy pid controller to control motor velocity.
        self.Ctl = Fuzzy_PID(10,7,4,2,1.15,0.75)
        self.Ctl.setKp(11,3)
        self.Ctl.setKi(10,0)
        self.Ctl.setKd(10,0.1)
        self.Ctl.setSampleTime(0.2)
        self.Ctl.setSetPoint(v_target)
        self.env = env
        
        # Initialize variables
        self.graph = []
        self.error = []
        self.Kps = []
        self.Kis = []
        self.Kds = []
        self.ISE = 0.0
        self.IAE = 0.0
        super().__init__(env)

    def pi(self, x, t=None):
        #feedback, thbot = self.env.state
        feedback, thbot = x[0], x[2]

        # Setting up potential plotting variables
        self.graph.append(feedback)
        error = self.Ctl.SetPoint - feedback + thbot/2
        self.error.append(error)
        self.ISE += (error) * 0.05
        self.IAE += (error)**2*0.05
        try:
            self.Kps.append(self.Ctl.Kp)
            self.Kis.append(self.Ctl.Ki)
            self.Kds.append(self.Ctl.Kd)
        except Exception:
            self.Kps.append(0)
            self.Kis.append(0)
            self.Kds.append(0)

        # Find next action to perform based on update step
        self.Ctl.update(feedback, thbot)
        action = self.Ctl.output
        if x[0] < 0:
            action = abs(action) * np.sign(x[2])
            #print(1, x, action)
        elif abs(1 - x[0]) < 0.4:
            action = -abs(action) * abs(x[2]) * np.sign(x[1])
            #print(2, x, action)
        action = np.clip(action, -5, 5)
        return [action]






class PIDPendulumAgent(Agent):
    def __init__(self, env, v_target=0.5):
        # Define pid controller to control motor velocity.
        self.pid = PID(dt = 0.05, Kp = 2, Ki = 0, Kd = 0.35, target = v_target)
        self.done_time = 0
        super().__init__(env)

    def pi(self, x, t=None):
        # Find action by calling the PID controller
        #time.sleep(0.1)

        if x[0] < 0:
            u = 30 * -self.pid.pi(x[0]) * np.sign(x[0]) * np.sign(x[2])
        elif abs(1 - x[0]) < 0.2:
            u = 100 * self.pid.pi(x[0]) * -np.sign(x[1])
        elif x[0] > 0:
            u = 30 * self.pid.pi(x[0]) * np.sign(x[0]) * np.sign(x[2])
        u = np.clip(u, -5, 5)
        if abs(1 - x[0]) < 0.01 and self.done_time == 0:
            self.done_time = t
        #print(f"x = {x} and u = {u} and t = {t}")
        #print([abs(1 - x[0]), u])
        return [u]

def plot_result(iterations, agent, env, is_fuzzy = False):
    fig, ax = plt.subplots(2)
    fig.set_size_inches(8, 4)
    ax1 = plt.subplot(211)
    ax2 = plt.subplot(212)
    for i in range(10):
        env.reset()
        agent = agent
        stats, trajectories = train(env, agent, num_episodes=1, return_trajectory=True, verbose = False)
        t = trajectories[0]
        #plt.plot(t.state[:,1], label="cos(theta)")
        ax1.plot(t.time[:120//2] * 0.05, t.state[:,0][:120//2], c = "green", linestyle = "-", alpha = 0.8)
        ax2.plot(t.time[:120//2] * 0.05, t.state[:,2][:120//2], c = "blue", linestyle = "-", alpha = 0.8)
        plt.xlabel("Time/seconds")
    plt.legend()
    ax1.set_title("Sin(Theta)")
    ax2.set_title("d theta/dt")
    plt.tight_layout()
    if is_fuzzy:
        plt.savefig(os.path.join(os.getcwd(), "RLprojects\project\imgs\pid_fuzzy_pendulum.png"))
    else:
        plt.savefig(os.path.join(os.getcwd(), "RLprojects\project\imgs\pid_pendulum.png"))
    plt.show()


if __name__ == "__main__":
    from irlc.ex01.agent import train
    from irlc.utils.video_monitor import VideoMonitor
    import matplotlib.pyplot as plt
    import gym

    # Initialize environment and agent
    env = gym.make('Pendulum-v0')
    env = VideoMonitor(env)

    # Running agent on environment
    plot_result(10, PIDPendulumAgent(env, v_target=1.0), env, is_fuzzy=False)

    plot_result(10, FuzzyPIDPendulumAgent(env, v_target = 1.1), env, is_fuzzy=True)