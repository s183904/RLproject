"""
This file may not be shared/redistributed without permission. Please read copyright notice in the git repo. If this file contains other copyright notices disregard this text.
"""
import os
import numpy as np
import matplotlib.pyplot as plt
np.random.seed(32)
from irlc import Agent, savepdf
from irlc.ex04.pid import PID
from irlc.ex01.agent import train
from irlc import VideoMonitor
from irlc.ex04.model_cartpole import GymThetaCartpoleEnvironment


class MultiplePIDCartpoleAgent(Agent):
    """
    Solves the cartpole problem using one PID controller to control the angle of the stick 
    and one to control the velocity of the cart.
    """
    def __init__(self, env, dt, Kp, Ki, Kd, target, balance_to_x0=True):
        p_crit = 1.27
        self.pid_velocity = PID(dt=dt, Kp=Kp[0] * p_crit, Ki=Ki[0] * p_crit, Kd=Kd[0] * p_crit, target=target[0])
        self.pid_angle =  PID(dt=dt, Kp=Kp[1] , Ki=Ki[1], Kd=Kd[1], target=target[1])
        self.balance_to_x0 = balance_to_x0
        super().__init__(env)

    def pi(self, x, t=None): 
        u = [0, 0]
        u[0] = 3.4 * self.pid_velocity.pi(- 2.81 * x[0] + 0.68 * x[1])
        u[1] = - 1 * self.pid_angle.pi(50 * x[2] - 1.2 * x[3])
        #print(f'x: {x[0]} and u: {np.clip(sum(u), -env.max_force, env.max_force)}')
        return np.clip(sum(u), -env.max_force, env.max_force)


class PIDCartpoleAgent(Agent):
    """
    Solves the cartpole problem using a standard single PID controller to control both angle and velocity.
    """
    def __init__(self, env, dt, Kp=1.0, Ki=0.0, Kd=0.0, target=0, balance_to_x0=True):
        self.pid = PID(dt=dt, Kp = Kp, Ki=Ki, Kd=Kd, target=target)
        self.balance_to_x0 = balance_to_x0
        super().__init__(env)

    def pi(self, x, t=None): 
        if self.balance_to_x0:
            # Part B
            # u = -self.pid.pi(50 * x[2] + 10 * x[0] - x[3])
            # Part C
            u = -self.pid.pi(120 * x[2] + 8 * x[0] - x[3]/2)
            u = np.clip(u, -env.max_force, env.max_force)
            #print(f"x: {x[0]} and u: {u}")
        else:
            u = -self.pid.pi(20 * x[2])
            u = np.clip(u, -env.max_force, env.max_force)
            #print(f"x: {x[2]} and u: {u}")
        return u

def get_offbalance_cart(waiting_steps=30):
    env = GymThetaCartpoleEnvironment(Tmax=15)
    env = VideoMonitor(env)
    env.reset()
    env.state[0] = 0
    env.state[1] = 0
    env.state[2] = 0  # Upright, but leaning slightly to one side.
    env.state[3] = 0
    for _ in range(waiting_steps):  # Simulate the environment for 30 steps to get things out of balance.
        env.step(1)
    return env

def plot_trajectory(trajectory, name, balance_to_x0 = True):
    t = trajectory
    color=next(ax._get_lines.prop_cycler)['color']
    if balance_to_x0:
        plt.plot(t.time, t.state[:,2], color, label= name + " angle $\\theta$" )
        plt.plot(t.time, t.state[:,0], color, linestyle = "--", label= name + " location $x$")
    else:
        plt.plot(t.time, t.state[:,2], color, label= name + " angle $\\theta$" )
    plt.xlabel("Time")
    plt.legend(loc = "lower right")

if __name__ == "__main__":
    # """
    # First task: Bring the balance upright from a slightly off-center position. 
    # For this task, we do not care about the x-position, only the angle theta which should be 0 (upright)
    # """
    my_path = os.getcwd()
    fig = plt.gcf()
    ax = plt.gca()
    fig.set_size_inches(8, 4)
    # Manual
    env = get_offbalance_cart(20)
    agent = PIDCartpoleAgent(env = env, dt = env.dt, Kp = 5, Ki = 0, Kd = 1, target = 0, balance_to_x0 = False)
    _, trajectories = train(env, agent, num_episodes=1, return_trajectory=True, reset=False)  # Note reset=False to maintain initial conditions.
    plot_trajectory(trajectories[0], "Manual", False)
    env.close()
    # Ziegler-Nichols
    # K_U = 2
    # P_U = 1.9
    env = get_offbalance_cart(20)
    agent = PIDCartpoleAgent(env = env, dt = env.dt, Kp = 0.6 * 6, Ki = 1.5 / 2, Kd = 1.5 / 8, target = 0, balance_to_x0 = False)
    _, trajectories = train(env, agent, num_episodes=1, return_trajectory=True, reset=False)
    plot_trajectory(trajectories[0], "ZN", False)
    env.close()
    # Added stackexchange
    env = get_offbalance_cart(20)
    agent = PIDCartpoleAgent(env = env, dt = env.dt, Kp = 4, Ki = 0.5, Kd = 1,   target = 0, balance_to_x0 = False)
    _, trajectories = train(env, agent, num_episodes=1, return_trajectory=True, reset=False)
    plot_trajectory(trajectories[0], "Stackexchange", False)
    plt.title("1ST Task")
    env.close()
    plt.savefig(os.path.join(my_path, "RLprojects\project\imgs\pid_cartpoleA.png"))
    plt.show()




    """
    Second task: We will now also try to bring the cart towards x=0.
    """
    fig = plt.gcf()
    ax = plt.gca()
    fig.set_size_inches(8, 4)
    # Manual
    env = get_offbalance_cart(20)
    agent = PIDCartpoleAgent(env = env, dt = env.dt, Kp = 4.8, Ki = 0.2, Kd = 0.22, target = 0, balance_to_x0 = True)
    _, trajectories = train(env, agent, num_episodes=1, return_trajectory=True, reset=False)
    plot_trajectory(trajectories[0], "Manual", True)
    env.close()
    # Ziegler-Nichols
    # K_U = 1
    # P_U = 0.9
    env = get_offbalance_cart(20)
    agent = PIDCartpoleAgent(env = env, dt = env.dt, Kp = 0.6 * 1, Ki = 0.9 / 2, Kd = 0.9 / 8, target = 0, balance_to_x0 = True)
    _, trajectories = train(env, agent, num_episodes=1, return_trajectory=True, reset=False)
    plot_trajectory(trajectories[0], "ZN", True)
    env.close()
    # Stackexchange
    env = get_offbalance_cart(20)
    agent = PIDCartpoleAgent(env = env, dt = env.dt, Kp = 6, Ki = 50, Kd = 0.65, target = 0, balance_to_x0 = True)
    _, trajectories = train(env, agent, num_episodes=1, return_trajectory=True, reset=False)
    plot_trajectory(trajectories[0], "Stackexchange", True)
    # Multiple PID
    env = get_offbalance_cart(20)
    agent = MultiplePIDCartpoleAgent(env, env.dt, Kp = [1, 6.2], Ki = [0.0, 0.88], Kd = [1.1,0.7], target=[-0.07, 0], balance_to_x0 = True)
    _, trajectories = train(env, agent, num_episodes=1, return_trajectory=True, reset=False)
    plot_trajectory(trajectories[0], "Manual Multiple", True)
    env.close()
    plt.title("2ND Task")
    env.close()
    plt.savefig(os.path.join(my_path, "RLprojects\project\imgs\pid_cartpoleB.png"))
    plt.show()




    """
    Third task: Bring the cart upright theta=0 and to the center x=0, but starting from a more challenging position. 
    """
    fig = plt.gcf()
    ax = plt.gca()
    fig.set_size_inches(8, 4)
    # Manual
    env = get_offbalance_cart(40)
    agent = PIDCartpoleAgent(env = env, dt = env.dt, Kp = 7, Ki = 0, Kd = 0.2, target = 0, balance_to_x0 = True)
    _, trajectories = train(env, agent, num_episodes=1, return_trajectory=True, reset=False)
    plot_trajectory(trajectories[0], "Manual", True)
    env.close()
    # Ziegler-Nichols
    # K_U = 30
    # P_U = 7
    env = get_offbalance_cart(40)
    agent = PIDCartpoleAgent(env = env, dt = env.dt, Kp = 0.6 * 30, Ki = 7 / 2, Kd = 7 / 8, target = 0, balance_to_x0 = True)
    _, trajectories = train(env, agent, num_episodes=1, return_trajectory=True, reset=False)
    plot_trajectory(trajectories[0], "ZN", True)
    env.close()
    # Stackexchange
    env = get_offbalance_cart(40)
    agent = PIDCartpoleAgent(env = env, dt = env.dt, Kp = 18, Ki = 10, Kd = 0.8, target = 0, balance_to_x0 = True)######
    # agent = PIDCartpoleAgent(env = env, dt = env.dt, Kp = 35, Ki = 30.2, Kd = 2, target = 0, balance_to_x0 = True)######
    _, trajectories = train(env, agent, num_episodes=1, return_trajectory=True, reset=False)
    plot_trajectory(trajectories[0], "Stackexchange", True)
    env.close()
    # Multiple PID
    env = get_offbalance_cart(40)
    agent = MultiplePIDCartpoleAgent(env, env.dt, Kp = [0.85, 5.2], Ki = [0, 0.88], Kd = [0.78,0.9], target=[-1.8880, 0], balance_to_x0 = True)
    _, trajectories = train(env, agent, num_episodes=1, return_trajectory=True, reset=False)
    plot_trajectory(trajectories[0], "Manual Multiple", True)
    env.close()

    plt.title("3RD Task")
    #plt.savefig(os.path.join(my_path, "RLprojects\project\imgs\pid_cartpoleC.png"))
    plt.show()

    
    #print(f"max cart: {max(abs(trajectories[0].state[:,0]))}\n",f"max angle {max(abs(trajectories[0].state[:,2]))}")
