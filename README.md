# Code used in the project
### Intro to Project and Codebase
This codebase aims to solve the pendulum and cartpole environments from AI Gym. the Pendulum environment is solved using standard PID and fuzzy PID, while cartpole is solved using standard PID and a PID controller for each output variable (multiple PID). 
The code included is a mix of code that has been written by the contributors of this project, as well as code used in the original course and the project code made by Zirui Zhao on [GitHub](https://github.com/1989Ryan/Fuzzy-Control-Project).

### Throrough Code Breakdown
The files `model_cartpole.py` and `pid_cartpole.py` have been used to solve the cartpole environment, while the pendulum task is of course solved using `model_pendulum.py` and `pid_pendulum.py`. The `model_pendulum.py` and `model_cartpole.py` have been adapted from the course material and used to prepare the necessary environment for running PID control. Meanwhile, `pid_cartpole.py` and `pid_pendulum.py`, while originally adapted from the course material, contains the bulk of new code written by us.
`pid_pendulum.py` uses the folder `fuzzy_project` which contains the code used to run fuzzy PID. All code in this folder is developed by Zirui Zhao, whose GitHub project can be found above.

All other imports in `pid_pendulum.py` and `pid_cartpole.py` are either common packages or code from the course material. The course material has not been included here, since no changes have been made to these files or functions.
We tried to get another fuzzy pid method from [medium](https://towardsdatascience.com/fuzzy-inference-system-implementation-in-python-8af88d1f0a6e) working, but abandoned the attempt when we found Zirui Zhao's GitHub page. Thus, this code has also not been included.